<div style='margin-top:40px' class="contenido">
<? if(empty($_SESSION['user'])): ?>
    <div class='col-xs-12 col-sm-5 col-sm-offset-4'><? $this->load->view('predesign/login'); ?></div>
<? else: ?>    
    <div class='row'>
        <div class='col-xs-12 col-sm-2' style="background:lightgray; padding-top:20px; padding-bottom:20px;">
            <ul class="nav nav-pills nav-stacked" role="tablist">
                <li>Panel de control</li>
                <li><a href="<?= base_url('admin/user') ?>"><i class="fa fa-user"></i> Usuarios</a></li>
                <li><a href="<?= base_url('admin/areas') ?>"><i class="fa fa-user"></i> Areas</a></li> 
                <li><a href="<?= base_url('admin/localidades') ?>"><i class="fa fa-user"></i> Localidades</a></li> 
                <li><a href="<?= base_url('admin/prioridades') ?>"><i class="fa fa-user"></i> Prioridades</a></li> 
                <li><a href="<?= base_url('admin/quejas') ?>"><i class="fa fa-user"></i> Quejas</a></li> 
            </ul>
        </div>
        <div class="col-xs-12 col-sm-10">
           <?php if(!empty($crud)): ?>
                <? $this->load->view('cruds/'.$crud); ?>
           <?php else: ?>
            <h1>Panel de control</h1>            
           <?php endif ?>
        </div>
    </div>    
<? endif; ?>
</div>