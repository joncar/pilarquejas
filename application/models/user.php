<?php

	class User extends CI_Model{
		var $log = false;

		function __construct(){
			parent::__construct();
			if(!empty($_SESSION['user']))
				$this->log = $_SESSION['user'];
		}

		function login($user,$pass)
		{
			$this->db->where('email',$user);
			$this->db->where('password',md5($pass));
			
			$r = $this->db->get('user');
			if($r->num_rows>0)
			{
                                $this->getParameters($r);
				return true;
			}
			else
				return false;
		}
		
		function login_short($id)
		{
			$this->getParameters($this->db->get_where('user',array('id'=>$id)));
		}
                
                function getParameters($row)
                {
                    $r = $row->row();
                    $_SESSION['user']=$r->id;                    
                    $_SESSION['username']=$r->username;
                }

		function unlog()
		{
			session_unset();
		}
		
		function edit($data)
		{
			$this->db->where('id',$_SESSION['user']);
			$this->db->update('user',$data);
		}

	}

?>