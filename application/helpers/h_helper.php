<?php 
	function  correo($email = 'joncar.c@gmail.com',$titulo = '',$msj = '',$from='Municipalidad de Pilar<info@pilar.gov.py>')
        {            
            $sfrom=$from; //cuenta que envia
            $sdestinatario=$email; //cuenta destino
            $ssubject=$titulo; //subject
            $shtml=$msj; //mensaje
            $shtml.='<b>Nota: Hemos quitado los acentos de este mail por motivos de visualizacion</b>';
            $sheader="From:".$sfrom."\nReply-To:".$sfrom."\n";
            $sheader=$sheader."X-Mailer:PHP/".phpversion()."\n";
            $sheader=$sheader."Mime-Version: 1.0\n";
            $sheader=$sheader."Content-Type: text/html; charset:utf-8";
            mail($sdestinatario,$ssubject,$shtml,$sheader);
        }
	
	
	function mail_activate($email,$name,$codigo_activacion)
	{
		$template = template_newusr_mail($name,$email,$codigo_activacion);
		correo($email,'Hola, '.$name.' gracias por registrarte, activa tu cuenta',$template);
	}
	
	function mail_recover($email,$codigo)
	{
		$template = template_recover_mail($email,$codigo);
		correo($email,'Solicitud de restablecimiento de contraseña',$template);
	}
	
	/********* templates mail ***********/
	function template_newusr_mail($name,$email,$codigo)
        {
	$ruta = base_url('registrar/activate/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Estimado '.$name.' gracias por registrarte en halfandhalf.es</h1>
	<h3>Puedes activar tu cuenta para poder publicar anuncios.</h3>
	<b>Pulsa en el enlace para activar tu cuenta <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	<p align="center"><a href="#">Politicas y condiciones de uso</a> | <a href="#">Contactenos</a></p>
	</div>
</div>';
        }
		
		
	function template_recover_mail($email,$codigo)
    {
	$ruta = base_url('inicio/recover/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Haz solicitado tu contraseña</h1>
	<b>Pulsa en el enlace para restablecer tu contraseña <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	</div>
</div>';
    }
    
    function paypal_button($product,$price)
    {
        return '
        <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="joncar.c@gmail.com">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="item_name" value="'.$product.'">
        <input type="hidden" name="amount" value="0">
        <button type="submit"><img src="'.base_url('img/paypal.jpg').'" style="width:100%; border-radius:1em"></button>
        </form>';               
    }
?>