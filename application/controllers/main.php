<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */        
	public function __construct()
	{
		parent::__construct();                
		$this->load->helper('url');
                $this->load->helper('html');
                $this->load->helper('h');                
                $this->load->database();
                $this->load->model('user');                        
                $this->load->model('querys');                        
                $this->load->library('grocery_crud');                
                $this->load->library('ajax_grocery_crud');
                ini_set('display_errors',true);                       
	}
        
        public function index($x = '')
	{
            if(empty($x))header("Location:".base_url('add'));
		$crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap');
                $crud->set_table('quejas');               
                $crud->set_subject('Quejas');   
                $crud->set_relation('localidad_id','localidad','denominacion');
                $crud->set_relation('area_id','areas','denominacion');
                $crud->set_relation('prioridad_id','prioridad','denominacion');
                $crud->unset_back_to_list();
                $crud->field_type('fechareclamo','invisible')
                     ->field_type('created','invisible')
                     ->field_type('modified','invisible');
                $crud->required_fields('cedula','nombres','apellidos','email','telef','localidad_id','ubicacion','problema','area_id','areas_id','prioridad_id');
                $crud->callback_before_insert(function($row){
                    $row['fechareclamo'] = date("Y-m-d H:i:s");
                    return $row;
                });
                
                $crud->callback_after_insert(function($row,$id){
                    //Enviar correo al quejon
                    $msj = '';
                    $msj .= '<h1>Hemos recibido su queja, le responderemos en cuanto tengamos una respuesta.</h1>';
                    $msj .= '<p>Su correo se ha enviado a los responsables de la queja, 
                            en cuanto tengan respuestas se le notificará, El id de su queja es: '.$id.' y para ver la traza de su queja puede consultarla en <a href="'.base_url('respuestas/'.$id).'">Resumen de la queja</a></p>';
                    correo($row['email'],'Hemos recibido su queja',$msj);
                    
                    $msj = '<h1>Se ha recibido una queja con los siguientes datos </h1>';
                    foreach($row as $r=>$x)
                    $msj.= '<p><b>'.$r.': </b> '.$x.'</p>';
                    
                    foreach(get_instance()->db->get_where('user',array('admin'=>1))->result() as $u)
                    correo($u->email,'Se recibio una queja',$msj);
                    
                    foreach(get_instance()->db->get_where('areas',array('id'=>$row['area_id']))->result() as $u)
                    correo($u->email,'Se recibio una queja',$msj);
                });
                $output = $crud->render();
                $output->view = 'main';
                $output->crud = 'user';                                
                $this->loadView($output);                
	}
        
        function respuestas($x = '',$y = '',$z = ''){
            if(!empty($x) && is_numeric($x) && $x>0 && $this->db->get_where('quejas',array('id'=>$x))->num_rows>0){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('respuestas');
            $crud->set_subject('Respuestas');
            if(empty($y) || $y=='success')$crud->set_relation('administrador_id','user','username');
            $crud->where('quejas_id',$x);
            $crud->columns('administrador_id','fecha','comentario');            
            $crud->required_fields('comentario');
            $crud->unset_fields('user_id');
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
            $output = $crud->render();
            $output->view = 'main';
            $output->crud = 'queja';
            $output->queja = $this->db->get_where('quejas',array('id'=>$x))->row();
            $this->loadView($output);               
            }
            else header("Location:".base_url());
        }
                
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['usuario']) && !empty($_POST['pass']))
			{
				$this->db->where('email',$this->input->post('usuario'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('usuario',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
                        
                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('admin'));
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                //$_SESSION['user'] = 'clean';
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
        
        function sendmsj()
        {
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            if($this->form_validation->run())                
            {
                $email = $this->querys->get_conf()->email_contacto;
                $msj = '';
                foreach($_POST as $i=>$p){
                    $msj.= '<p><b>'.$i.'</b>: '.$p.'</p>';
                }
                correo($email,$_POST['motivo'],$msj);
                echo $this->success('Su correo ha sido enviado, en breve le contactare');
            }
            else echo $this->error($this->form_validation->error_string());
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */