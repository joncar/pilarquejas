<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('registro/conectar'));
                $this->load->library('image_crud');
	}
        
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('panel');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url());
            else
            parent::loadView($crud);
        }                
        /*Cruds*/               
}   
    

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */