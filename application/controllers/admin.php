<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
		parent::__construct();               
	}
        
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('panel');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url());
            else
            parent::loadView($crud);
        }                
        
        public function user()
	{            
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('user');               
            $crud->set_subject('Usuario');                   
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'))
                 ->field_type('admin','true_false',array('0'=>'No','1'=>'Si'))
                 ->field_type('password','password');
            $crud->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
            $crud->columns('username','email','create_time','status','admin');
            $crud->required_fields('username','email','create_time','status','admin');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';                                
            $this->loadView($output);                
	}
        
        function areas(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('areas');               
            $crud->set_subject('Areas');   
            $crud->unset_delete()
                    ->unset_read();
            $crud->required_fields('denominacion','email','celular');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';                                
            $this->loadView($output);                
        }
        
        function localidades(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('localidad');               
            $crud->set_subject('Localidad');   
            $crud->unset_delete()
                    ->unset_read();
            $crud->required_fields('denominacion');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';                                
            $this->loadView($output);                
        }
        
        function prioridades(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('prioridad');               
            $crud->set_subject('Prioridad');   
            $crud->unset_delete()
                    ->unset_read();
            $crud->required_fields('denominacion');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';                                
            $this->loadView($output);                
        }
        
        function quejas(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('quejas');               
            $crud->set_subject('Quejas');  
            $crud->set_relation('area_id','areas','denominacion');
            $crud->unset_delete()
                    ->unset_read();         
            $crud->columns('cedula','nombres','apellidos','email','area_id','fechareclamo','problema');
            $crud->callback_column('problema',function($val,$row){
                return '<a href="'.base_url('admin/respuestas/'.$row->id).'">'.substr($val,0,50).'...</a>';
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';                                
            $this->loadView($output);                
        }
        
        function respuestas($x = '',$y = '',$z = ''){
            if(!empty($x) && is_numeric($x) && $x>0){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('respuestas');
            $crud->set_subject('Respuestas');            
            if(empty($y) || $y=='success')$crud->set_relation('administrador_id','user','username');
            $crud->where('quejas_id',$x);
            $crud->columns('administrador_id','fecha','comentario');
            $crud->field_type('quejas_id','hidden',$x);
            $crud->field_type('administrador_id','hidden',$_SESSION['user']);
            $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
            $crud->required_fields('comentario');
            $crud->unset_fields('user_id');
            $crud->unset_delete()
                 ->unset_read();            
            $crud->callback_after_insert(function($row,$id){
                //Enviar correo al quejon
                    $u = get_instance()->db->get_where('quejas',array('id'=>$row['quejas_id']))->row();
                    $msj = '';
                    $msj .= '<h1>Señor/a. '.$u->nombres.' '.$u->apellidos.' Se ha respondido su queja de ID: '.$row['quejas_id'].'.</h1>';
                    $msj .= '<p> para ver la traza de su queja puede consultarla en <a href="'.base_url('respuestas/'.$row['quejas_id']).'">Resumen de la queja</a></p>';
                    $correo = $u->email;
                    correo($correo,'Se ha respondido su queja Nro. '.$row['quejas_id'],$msj);
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'queja';
            $output->queja = $this->db->get_where('quejas',array('id'=>$x))->row();
            $this->loadView($output);               
            }
            else header("Location:".base_url('admin/quejas'));
        }
        /*Cruds*/               
}   
    

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */